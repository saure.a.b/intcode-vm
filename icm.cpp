#include "icm.h"
#include <sys/mman.h>
#include <utility>
#include <cstdint>
#include <fstream>
#include <algorithm>
#include <iterator>
#include <inttypes.h>

using ictstream_iterator = std::istream_iterator<icm::intcode_tok<std::int64_t>>;

namespace icm {
  vm::vm(vm &&old)
    : _bp   (std::exchange(old._bp   , nullptr))
    , _ip   (std::exchange(old._ip   , nullptr))
    , _rp   (std::exchange(old._rp   , nullptr))
    , _flag (std::exchange(old._flag , 0)) {}

  vm& vm::operator=(vm &&old) {
    if(this != &old) {
      if (_bp != nullptr)
        munmap(_bp, memsize);
      _bp    = std::exchange(old._bp   , nullptr);
      _ip    = std::exchange(old._ip   , nullptr);
      _rp    = std::exchange(old._rp   , nullptr);
      _flag  = std::exchange(old._flag , 0);
    }
    return *this;
  }

  vm::~vm() {
    if (_bp != nullptr)
      munmap(_bp, memsize);
  }

  void vm::load_file(const char *path) {
    std::ifstream in(path);
    load_range(ictstream_iterator(in), ictstream_iterator());
  }

  void vm::run() {
    void *opcodes[22209];
    std::int64_t *bp = _bp, *rp = _rp, *ip = _ip;

    _flag = 0;

    opcodes[    1] =     &&op_1;
    opcodes[  101] =   &&op_101;
    opcodes[  201] =   &&op_201;
    opcodes[ 1001] =  &&op_1001;
    opcodes[ 1101] =  &&op_1101;
    opcodes[ 1201] =  &&op_1201;
    opcodes[ 2001] =  &&op_2001;
    opcodes[ 2101] =  &&op_2101;
    opcodes[ 2201] =  &&op_2201;
    opcodes[20001] = &&op_20001;
    opcodes[20101] = &&op_20101;
    opcodes[20201] = &&op_20201;
    opcodes[21001] = &&op_21001;
    opcodes[21101] = &&op_21101;
    opcodes[21201] = &&op_21201;
    opcodes[22001] = &&op_22001;
    opcodes[22101] = &&op_22101;
    opcodes[22201] = &&op_22201;

    opcodes[    2] =     &&op_2;
    opcodes[  102] =   &&op_102;
    opcodes[  202] =   &&op_202;
    opcodes[ 1002] =  &&op_1002;
    opcodes[ 1102] =  &&op_1102;
    opcodes[ 1202] =  &&op_1202;
    opcodes[ 2002] =  &&op_2002;
    opcodes[ 2102] =  &&op_2102;
    opcodes[ 2202] =  &&op_2202;
    opcodes[20002] = &&op_20002;
    opcodes[20102] = &&op_20102;
    opcodes[20202] = &&op_20202;
    opcodes[21002] = &&op_21002;
    opcodes[21102] = &&op_21102;
    opcodes[21202] = &&op_21202;
    opcodes[22002] = &&op_22002;
    opcodes[22102] = &&op_22102;
    opcodes[22202] = &&op_22202;

    opcodes[    3] =     &&op_3;
    opcodes[  103] =   &&op_103;
    opcodes[  203] =   &&op_203;

    opcodes[    4] =     &&op_4;
    opcodes[  104] =   &&op_104;
    opcodes[  204] =   &&op_204;

    opcodes[    5] =     &&op_5;
    opcodes[  105] =   &&op_105;
    opcodes[  205] =   &&op_205;
    opcodes[ 1005] =  &&op_1005;
    opcodes[ 1105] =  &&op_1105;
    opcodes[ 1205] =  &&op_1205;
    opcodes[ 2005] =  &&op_2005;
    opcodes[ 2105] =  &&op_2105;
    opcodes[ 2205] =  &&op_2205;

    opcodes[    6] =     &&op_6;
    opcodes[  106] =   &&op_106;
    opcodes[  206] =   &&op_206;
    opcodes[ 1006] =  &&op_1006;
    opcodes[ 1106] =  &&op_1106;
    opcodes[ 1206] =  &&op_1206;
    opcodes[ 2006] =  &&op_2006;
    opcodes[ 2106] =  &&op_2106;
    opcodes[ 2206] =  &&op_2206;

    opcodes[    7] =     &&op_7;
    opcodes[  107] =   &&op_107;
    opcodes[  207] =   &&op_207;
    opcodes[ 1007] =  &&op_1007;
    opcodes[ 1107] =  &&op_1107;
    opcodes[ 1207] =  &&op_1207;
    opcodes[ 2007] =  &&op_2007;
    opcodes[ 2107] =  &&op_2107;
    opcodes[ 2207] =  &&op_2207;
    opcodes[20007] = &&op_20007;
    opcodes[20107] = &&op_20107;
    opcodes[20207] = &&op_20207;
    opcodes[21007] = &&op_21007;
    opcodes[21107] = &&op_21107;
    opcodes[21207] = &&op_21207;
    opcodes[22007] = &&op_22007;
    opcodes[22107] = &&op_22107;
    opcodes[22207] = &&op_22207;

    opcodes[    8] =     &&op_8;
    opcodes[  108] =   &&op_108;
    opcodes[  208] =   &&op_208;
    opcodes[ 1008] =  &&op_1008;
    opcodes[ 1108] =  &&op_1108;
    opcodes[ 1208] =  &&op_1208;
    opcodes[ 2008] =  &&op_2008;
    opcodes[ 2108] =  &&op_2108;
    opcodes[ 2208] =  &&op_2208;
    opcodes[20008] = &&op_20008;
    opcodes[20108] = &&op_20108;
    opcodes[20208] = &&op_20208;
    opcodes[21008] = &&op_21008;
    opcodes[21108] = &&op_21108;
    opcodes[21208] = &&op_21208;
    opcodes[22008] = &&op_22008;
    opcodes[22108] = &&op_22108;
    opcodes[22208] = &&op_22208;

    opcodes[    9] =     &&op_9;
    opcodes[  109] =   &&op_109;
    opcodes[  209] =   &&op_209;

    opcodes[   99] =    &&op_99;

#define DSP(ptr) goto *opcodes[*(ptr)]
#define IMM(n) ip[n]
#define POS(n) bp[ip[n]]
#define REL(n) rp[ip[n]]

    DSP(ip);
    
  op_1    : POS(3) = POS(1) + POS(2); DSP(ip += 4);
  op_101  : POS(3) = IMM(1) + POS(2); DSP(ip += 4);
  op_201  : POS(3) = REL(1) + POS(2); DSP(ip += 4);
  op_1001 : POS(3) = POS(1) + IMM(2); DSP(ip += 4);
  op_1101 : POS(3) = IMM(1) + IMM(2); DSP(ip += 4);
  op_1201 : POS(3) = REL(1) + IMM(2); DSP(ip += 4);
  op_2001 : POS(3) = POS(1) + REL(2); DSP(ip += 4);
  op_2101 : POS(3) = IMM(1) + REL(2); DSP(ip += 4);
  op_2201 : POS(3) = REL(1) + REL(2); DSP(ip += 4);
  op_20001: REL(3) = POS(1) + POS(2); DSP(ip += 4);
  op_20101: REL(3) = IMM(1) + POS(2); DSP(ip += 4);
  op_20201: REL(3) = REL(1) + POS(2); DSP(ip += 4);
  op_21001: REL(3) = POS(1) + IMM(2); DSP(ip += 4);
  op_21101: REL(3) = IMM(1) + IMM(2); DSP(ip += 4);
  op_21201: REL(3) = REL(1) + IMM(2); DSP(ip += 4);
  op_22001: REL(3) = POS(1) + REL(2); DSP(ip += 4);
  op_22101: REL(3) = IMM(1) + REL(2); DSP(ip += 4);
  op_22201: REL(3) = REL(1) + REL(2); DSP(ip += 4);

  op_2    : POS(3) = POS(1) * POS(2); DSP(ip += 4);
  op_102  : POS(3) = IMM(1) * POS(2); DSP(ip += 4);
  op_202  : POS(3) = REL(1) * POS(2); DSP(ip += 4);
  op_1002 : POS(3) = POS(1) * IMM(2); DSP(ip += 4);
  op_1102 : POS(3) = IMM(1) * IMM(2); DSP(ip += 4);
  op_1202 : POS(3) = REL(1) * IMM(2); DSP(ip += 4);
  op_2002 : POS(3) = POS(1) * REL(2); DSP(ip += 4);
  op_2102 : POS(3) = IMM(1) * REL(2); DSP(ip += 4);
  op_2202 : POS(3) = REL(1) * REL(2); DSP(ip += 4);
  op_20002: REL(3) = POS(1) * POS(2); DSP(ip += 4);
  op_20102: REL(3) = IMM(1) * POS(2); DSP(ip += 4);
  op_20202: REL(3) = REL(1) * POS(2); DSP(ip += 4);
  op_21002: REL(3) = POS(1) * IMM(2); DSP(ip += 4);
  op_21102: REL(3) = IMM(1) * IMM(2); DSP(ip += 4);
  op_21202: REL(3) = REL(1) * IMM(2); DSP(ip += 4);
  op_22002: REL(3) = POS(1) * REL(2); DSP(ip += 4);
  op_22102: REL(3) = IMM(1) * REL(2); DSP(ip += 4);
  op_22202: REL(3) = REL(1) * REL(2); DSP(ip += 4);

  op_3    : input(POS(1)); if(!_flag) DSP(ip += 2); else goto end;
  op_103  : input(IMM(1)); if(!_flag) DSP(ip += 2); else goto end;
  op_203  : input(REL(1)); if(!_flag) DSP(ip += 2); else goto end;

  op_4    : output(POS(1)); if(!_flag) DSP(ip += 2); else goto end;
  op_104  : output(IMM(1)); if(!_flag) DSP(ip += 2); else goto end;
  op_204  : output(REL(1)); if(!_flag) DSP(ip += 2); else goto end;

  op_5    : DSP(ip = POS(1) ? bp + POS(2) : ip + 3);
  op_105  : DSP(ip = IMM(1) ? bp + POS(2) : ip + 3);
  op_205  : DSP(ip = REL(1) ? bp + POS(2) : ip + 3);
  op_1005 : DSP(ip = POS(1) ? bp + IMM(2) : ip + 3);
  op_1105 : DSP(ip = IMM(1) ? bp + IMM(2) : ip + 3);
  op_1205 : DSP(ip = REL(1) ? bp + IMM(2) : ip + 3);
  op_2005 : DSP(ip = POS(1) ? bp + REL(2) : ip + 3);
  op_2105 : DSP(ip = IMM(1) ? bp + REL(2) : ip + 3);
  op_2205 : DSP(ip = REL(1) ? bp + REL(2) : ip + 3);

  op_6    : DSP(ip = POS(1) ? ip + 3 : bp + POS(2));
  op_106  : DSP(ip = IMM(1) ? ip + 3 : bp + POS(2));
  op_206  : DSP(ip = REL(1) ? ip + 3 : bp + POS(2));
  op_1006 : DSP(ip = POS(1) ? ip + 3 : bp + IMM(2));
  op_1106 : DSP(ip = IMM(1) ? ip + 3 : bp + IMM(2));
  op_1206 : DSP(ip = REL(1) ? ip + 3 : bp + IMM(2));
  op_2006 : DSP(ip = POS(1) ? ip + 3 : bp + REL(2));
  op_2106 : DSP(ip = IMM(1) ? ip + 3 : bp + REL(2));
  op_2206 : DSP(ip = REL(1) ? ip + 3 : bp + REL(2));

  op_7    : POS(3) = POS(1) < POS(2); DSP(ip += 4);
  op_107  : POS(3) = IMM(1) < POS(2); DSP(ip += 4);
  op_207  : POS(3) = REL(1) < POS(2); DSP(ip += 4);
  op_1007 : POS(3) = POS(1) < IMM(2); DSP(ip += 4);
  op_1107 : POS(3) = IMM(1) < IMM(2); DSP(ip += 4);
  op_1207 : POS(3) = REL(1) < IMM(2); DSP(ip += 4);
  op_2007 : POS(3) = POS(1) < REL(2); DSP(ip += 4);
  op_2107 : POS(3) = IMM(1) < REL(2); DSP(ip += 4);
  op_2207 : POS(3) = REL(1) < REL(2); DSP(ip += 4);
  op_20007: REL(3) = POS(1) < POS(2); DSP(ip += 4);
  op_20107: REL(3) = IMM(1) < POS(2); DSP(ip += 4);
  op_20207: REL(3) = REL(1) < POS(2); DSP(ip += 4);
  op_21007: REL(3) = POS(1) < IMM(2); DSP(ip += 4);
  op_21107: REL(3) = IMM(1) < IMM(2); DSP(ip += 4);
  op_21207: REL(3) = REL(1) < IMM(2); DSP(ip += 4);
  op_22007: REL(3) = POS(1) < REL(2); DSP(ip += 4);
  op_22107: REL(3) = IMM(1) < REL(2); DSP(ip += 4);
  op_22207: REL(3) = REL(1) < REL(2); DSP(ip += 4);

  op_8    : POS(3) = POS(1) == POS(2); DSP(ip += 4);
  op_108  : POS(3) = IMM(1) == POS(2); DSP(ip += 4);
  op_208  : POS(3) = REL(1) == POS(2); DSP(ip += 4);
  op_1008 : POS(3) = POS(1) == IMM(2); DSP(ip += 4);
  op_1108 : POS(3) = IMM(1) == IMM(2); DSP(ip += 4);
  op_1208 : POS(3) = REL(1) == IMM(2); DSP(ip += 4);
  op_2008 : POS(3) = POS(1) == REL(2); DSP(ip += 4);
  op_2108 : POS(3) = IMM(1) == REL(2); DSP(ip += 4);
  op_2208 : POS(3) = REL(1) == REL(2); DSP(ip += 4);
  op_20008: REL(3) = POS(1) == POS(2); DSP(ip += 4);
  op_20108: REL(3) = IMM(1) == POS(2); DSP(ip += 4);
  op_20208: REL(3) = REL(1) == POS(2); DSP(ip += 4);
  op_21008: REL(3) = POS(1) == IMM(2); DSP(ip += 4);
  op_21108: REL(3) = IMM(1) == IMM(2); DSP(ip += 4);
  op_21208: REL(3) = REL(1) == IMM(2); DSP(ip += 4);
  op_22008: REL(3) = POS(1) == REL(2); DSP(ip += 4);
  op_22108: REL(3) = IMM(1) == REL(2); DSP(ip += 4);
  op_22208: REL(3) = REL(1) == REL(2); DSP(ip += 4);

  op_9    : rp += POS(1); DSP(ip += 2);
  op_109  : rp += IMM(1); DSP(ip += 2);
  op_209  : rp += REL(1); DSP(ip += 2);

  op_99   : _flag = 1;

  end:
    _bp = bp;
    _ip = ip;
    _rp = rp;
  }
#undef DSP
#undef POS
#undef IMM
#undef REL
}
