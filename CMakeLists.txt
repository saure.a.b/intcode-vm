cmake_minimum_required(VERSION 3.10)

project(icm)

set(CMAKE_CXX_COMPILER "clang++")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O3 -std=c++17")

add_library(icm STATIC icm.cpp)
