#pragma once
#include <cstdint>
#include <algorithm>
#include <iterator>
#include <sys/mman.h>
#include <iostream>

namespace icm {
  template<typename T, char sep = ','>
  struct intcode_tok {
    T t;
    operator const T&() const { return t; }

    friend std::istream& operator>>(std::istream& in, intcode_tok<T, sep>& t) {
      if(!(in >> t.t));
      else if(in.peek() == sep) in.ignore();
      else in.clear();
      return in;
    }
  };
  
  class vm {
    std::int64_t *_bp = nullptr, *_ip, *_rp;
    virtual void input(std::int64_t &r) { std::cin >> r; }
    virtual void output(std::int64_t a) { std::cout << a << '\n'; }

  protected:
    int _flag = 1;
    
  public:
    static constexpr int memsize = 1 << 29;
    int flag() noexcept { return _flag; }
    std::int64_t* bp() noexcept { return _bp; }
    std::int64_t* ip() noexcept { return _rp; }
    std::int64_t* rp() noexcept { return _ip; }
    void run();
    void load_file(const char *);
    
    template<typename InputIt>
    void load_range(InputIt begin, InputIt end) {
      if (_bp != nullptr)
        munmap(_bp, memsize);
      _bp = _rp = _ip = static_cast<std::int64_t*>
        (mmap(NULL, memsize, PROT_READ | PROT_WRITE,
              MAP_ANONYMOUS | MAP_PRIVATE | MAP_NORESERVE, 0, 0));
      std::copy(begin, end, _ip);
    }

    vm() noexcept {}
    vm(const vm &) = delete;
    vm(vm &&);
    
    vm(const char *path) { load_file(path); }
    
    template<typename InputIt>
    vm(InputIt begin, InputIt end) { load_range(begin, end); }

    vm& operator=(const vm &) = delete;
    vm& operator=(vm &&);

    ~vm();
  };
}
